Install on VM not LXC

Using Ubuntu 22.04 VM on Proxmox

Optional:
Change Hostname
```yaml
nano /etc/hostname
```

View Current IP Address
```yaml
ip addr
```
Set Static IP Address
```yaml
nano /etc/netplan/01-network-manager-all.yaml
```
```yaml
network:
version: 2
renderer: NetworkManager
ethernets:
 ens33:
  dhcp4: no
  addresses:
  - 192.168.72.140/24
  gateway4: 192.168.72.2
  nameservers:
   addresses: [8.8.8.8, 8.8.4.4]
```
```yaml
netplan apply
```






```yaml
sudo -i

apt update
apt upgrade
```

Remove-disable swap
```yaml
swapoff -a
rm /swap.img
```
Remove following line from /etc/fstab
```yaml
/swap.img       none    swap    sw      0       0
```

Disable Firewall
ufw status