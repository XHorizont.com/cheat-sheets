!!! LXC Enable nesting !!!

Ubuntu 20.04, 22.04




```plaintext
apt update
apt upgrade

apt install softawe-properties-common curl gnupg2 wget git unzip ca-certificates -y

mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

  apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

systemctl start docker
systemctl enable docker
systemctl status docker

docker run hello-world

apt install docker-compose

docker-compose --version

apt install ansible -y

ansible --version

apt install nodejs npm -y

npm install npm --global

apt install python3-pip pwgen -y

pip3 install docker-compose==1.25.0

wget https://github.com/ansible/awx/archive/refs/tags/17.1.0.zip

unzip 17.1.0.zip

cd awx-17.1.0

cd installer

pwgen -N 1 -s 30

nano inventory

*** add ***

admin_user=admin
admin_password=password
secret_key=0SDmXawLP671J5FH8ugjzEz2RthqUq

***

ansible-playbook -i inventory install.yml

docker ps

```


If containers are running AWX shoud be available on system IP