As sudo

```plaintext
apt update
apt upgrade

mkdir /opt/netbox

useradd -d /opt/netbox netbox -s /bin/bash

usermod -aG sudo netbox

chown -R netbox:netbox /opt/netbox

apt install -y git gcc nginx redis python3-setuptools  graphviz python3 python3-pip python3-venv python3-dev build-essential libxml2-dev libxslt1-dev libffi-dev libpq-dev libssl-dev zlib1g-dev
```
Install PostgreSQL Database Server

```plaintext
apt install postgresql postgresql-client -y
systemctl status postgresql.service 
sudo su - postgres
psql -c "alter user postgres with password 'MySt0ngDBP@ss'"

```
```plaintext
sudo -u postgres psql

CREATE DATABASE netbox;

CREATE USER netbox WITH PASSWORD 'StrongPassword';

GRANT ALL PRIVILEGES ON DATABASE netbox TO netbox;

\q
```

Confirm that you can login to database as netbox user.
```plaintext
psql -U netbox -h localhost -W
Password: <Enter-Password>
psql (14.5 (Ubuntu 14.5-0ubuntu0.22.04.1))
SSL connection (protocol: TLSv1.3, cipher: TLS_AES_256_GCM_SHA384, bits: 256, compression: off)
Type "help" for help.

netbox=> exit
```
```plaintext
sudo su - netbox
git clone -b master https://github.com/digitalocean/netbox.git
cd netbox/netbox/netbox/
cp configuration_example.py configuration.py
nano configuration.py
```
```plaintext

# Example: ALLOWED_HOSTS = ['netbox.example.com', 'netbox.internal.local']
ALLOWED_HOSTS = ['localhost','127.0.0.1']
#ALLOWED_HOSTS = ['*']

# PostgreSQL database configuration.
DATABASE = {
    'NAME': 'netbox',                           # Database name you created
    'USER': 'netbox',                           # PostgreSQL username you created
    'PASSWORD': 'StrongPassword',               # PostgreSQL password you set
    'HOST': 'localhost',                        # Database server
    'PORT': '',                                 # Database port (leave blank for default)
}
```

Set Up Python Environment
```plaintext
cd ~/

python3 -m venv /opt/netbox/netbox/venv

cd /opt/netbox/netbox/

source venv/bin/activate

pip3 install -r requirements.txt
```

```plaintext
cd /opt/netbox/netbox/netbox
./generate_secret_key.py
/opt/netbox/netbox/netbox/netbox/configuration.py (SECRET_KEY =)

cd /opt/netbox/netbox/netbox
python3 manage.py migrate
python3 manage.py createsuperuser
pip3 install gunicorn

cp /opt/netbox/netbox/contrib/gunicorn.py /opt/netbox/netbox/gunicorn.py
nano /opt/netbox/netbox/gunicorn.py

#Add below lines
user = 'netbox'
command = '/opt/netbox/netbox/venv/bin/gunicorn'
pythonpath = '/opt/netbox/netbox/netbox'

sudo cp /opt/netbox/netbox/contrib/*.service /etc/systemd/system/

sudo tee /etc/systemd/system/netbox.service<<EOF
[Unit]
Description=NetBox WSGI Service
Documentation=https://docs.netbox.dev/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple

User=netbox
Group=netbox
PIDFile=/var/tmp/netbox.pid
WorkingDirectory=/opt/netbox/netbox

ExecStart=/opt/netbox/netbox/venv/bin/gunicorn --pid /var/tmp/netbox.pid --pythonpath /opt/netbox/netbox/netbox --config /opt/netbox/netbox/gunicorn.py netbox.wsgi

Restart=on-failure
RestartSec=30
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

sudo tee /etc/systemd/system/netbox-rq.service<<EOF
[Unit]
Description=NetBox Request Queue Worker
Documentation=https://docs.netbox.dev/
After=network-online.target
Wants=network-online.target

[Service]
Type=simple

User=netbox
Group=netbox
WorkingDirectory=/opt/netbox/netbox

ExecStart=/opt/netbox/netbox/venv/bin/python3 /opt/netbox/netbox/netbox/manage.py rqworker high default low

Restart=on-failure
RestartSec=30
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOF

sudo chown -R netbox:netbox /opt/netbox

sudo systemctl daemon-reload
sudo systemctl enable netbox netbox-rq
sudo systemctl restart netbox netbox-rq

systemctl status netbox netbox-rq

## Back up the default file
cd /etc/nginx/sites-enabled/
sudo mv default /tmp

## Create a new file for Netbox
sudo vim /etc/nginx/sites-enabled/netbox.conf

server {
    listen 80;
    server_name netbox.computingforgeeks.com;
    client_max_body_size 25m;

    location /static/ {
        alias /opt/netbox/netbox/netbox/static/;
    }

    location / {
        proxy_pass http://localhost:8001;
        proxy_set_header X-Forwarded-Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}

sudo systemctl restart nginx

nano /etc/hosts

add <IP> FQDN
172.28.218.207 netbox.xdata.com

```
Open your default web browser and open the Netbox server hostname. 
